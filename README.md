# Pizza restaurant - Play framework

## Useful links
- [Play framework documentation](https://www.playframework.com/documentation/3.0.x/Home)
- [Bootstrap documentation](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
***

## Motivation

I wanted to explore new technology, and after reading about the Play framework, 
I decided to create a very simple app.

I decided to use Bootstrap as the CSS library for this project, as I have experience with it from previous projects.

***
## Goals
- [x] Develop a simple welcome page.
- [x] The welcome page includes a button that redirects the user to the menu page
- [x] Develop a simple menu page.
- [x] The menu page must include an "Add to Cart" button.
- [x] The "Add to Cart" button should increment the pizza counter upon clicking

***
## How to run
- Prerequisites - https://www.playframework.com/documentation/3.0.x/Requirements
- Run command: sbt run
***

## Result

### Simple welcome page
![welcome page](img/blaf-1.png "Simple welcome page")

### Simple menu page
![menu page](img/blaf-2.png "Simple menu page")


