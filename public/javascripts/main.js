
    const addToCartButtons = document.querySelectorAll('.addToCartBtn');
    const cartText = document.getElementById("cart")
    var pizzaCounter = 0;

    addToCartButtons.forEach(button => {
        button.addEventListener('click', function(event) {
            event.preventDefault();
            const pizzaData = this.getAttribute('data-pizza');
            addToCart(pizzaData);
        });
    });

    function addToCart(pizza) {
        console.log(pizza);
        pizzaCounter++;
        cartText.textContent = pizzaCounter;

    }