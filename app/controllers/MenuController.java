package controllers;

import jakarta.inject.Inject;
import play.mvc.Controller;
import play.mvc.Result;
import service.PizzaMenuService;

/**
 * Returns pizza menu
 */
public class MenuController extends Controller {

    private final PizzaMenuService pizzaMenuService;

    @Inject
    public MenuController(PizzaMenuService pizzaMenuService) {
        this.pizzaMenuService = pizzaMenuService;
    }

    public Result menu() {

        return ok(views.html.menu.render(pizzaMenuService.getAllPizzas()));
    }
}
