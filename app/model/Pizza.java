package model;

public record Pizza(String name, String description) {
}
