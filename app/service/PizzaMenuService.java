package service;

import model.Pizza;

import java.util.List;

public class PizzaMenuService {

    public List<Pizza> getAllPizzas() {
        return List.of(
                new Pizza("Hawai", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse urna metus, pharetra vel suscipit sit amet, ornare id nibh. Aliquam"),
                new Pizza("Funghi", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse urna metus, pharetra vel suscipit sit amet, ornare id nibh. Aliquam"),
                new Pizza("Margherita", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse urna metus, pharetra vel suscipit sit amet, ornare id nibh. Aliquam")
        );
    }
}
